package com.week6.question2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author Bhupesh Shrestha
 * Date: 9/25/2020
 * Q.4
 * Q.5 a,b,c
 *
 */

  public class EmployeeUtil{

	//Method to calculate the highest salary  
    public static String highestSalary(String name1, String name2,double salary1, double salary2) {
    	
    
    	if(salary1 > salary2) {
    		System.out.println(name1 + " has more salary than " + name2);
    	    return name1;
    	}
    	else {
    		System.out.println(name2 + " has more salary than " + name1);
    		return name2;
    	}
    	
    }
    
    //method to increase salary by 10%
    public static void increaseSalary(double salary) {
    	double newSalary = salary * 10/100;
    	System.out.println("The salary after 10% increase salary is: " + (newSalary + salary) + "\n");
    	
    }
    
    //method to calculate the older employee
    //Q.5a
    public static String olderEmployee(Employee employee1, Employee employee2) {
    	if(employee1.getAge() > employee2.getAge()) {
   		System.out.println(employee1.getFirstName() + " is older than " + employee2.getFirstName());
    	    
    	}
    	else {
    		System.out.println(employee2.getFirstName() + " is older than " + employee1.getFirstName());
    		
    	}
    	return null;
    }
    
    //method to update the given employee salary
    //Q.5b
    public static void updateSalary(List<Employee> employee) {
    	
    	for(Employee Employees: employee) {
			
			if(Employees.getSalary() < 10000 && Employees.getAge() > 35) {
	    		System.out.println(Employees.getFirstName() + " increased salary is " + (Employees.getSalary() * 15/100 + Employees.getSalary()));
	    	}
	    	else if(Employees.getSalary() < 15000 && Employees.getAge() > 45) {
	    		System.out.println(Employees.getFirstName() + " increased salary is " + (Employees.getSalary() * 15/100 + Employees.getSalary()));
	    	}
	    	else if(Employees.getSalary() < 20000 && Employees.getAge() > 55) {
	    		System.out.println(Employees.getFirstName() + " increased salary is " + (Employees.getSalary() * 15/100 + Employees.getSalary()));
	    	}
		}
    }
    
    //Q.5c
    public static void calculateGrossSalary(List<Employee> employee) {
    	
    	for(Employee Employees: employee) {
    		
    		if(Employees.getSalary() < 10000) {
    			System.out.println(Employees.getFirstName() + " gross salary is: " + (Employees.getSalary() * 15/100 + Employees.getSalary() * 8/100 + Employees.getSalary()));
    		}
    		else if(Employees.getSalary() > 10000 && Employees.getSalary() < 20000) {
    			System.out.println(Employees.getFirstName() + " gross salary is: " + (Employees.getSalary() * 10/100 + Employees.getSalary() * 20/100 + Employees.getSalary()));
    		}
    		else if(Employees.getSalary() < 30000 && Employees.getAge() >= 40) {
    			System.out.println(Employees.getFirstName() + " gross salary is: " + (Employees.getSalary() * 15/100 + Employees.getSalary() * 27/100 + Employees.getSalary()));
    		}
    		else if(Employees.getSalary() < 30000 && Employees.getAge() < 40) {
    			System.out.println(Employees.getFirstName() + " gross salary is: " + (Employees.getSalary() * 13/100 + Employees.getSalary() * 25/100 + Employees.getSalary()));
    		}
    		else {
    			System.out.println(Employees.getFirstName() + " gross salary is: " + (Employees.getSalary() * 17/100 + Employees.getSalary() * 30/100 + Employees.getSalary()));
    		}
    	}
    }
    
}
