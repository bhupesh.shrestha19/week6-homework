package com.week6.question2;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Bhupesh Shrestha
 * Date: 9/25/2020
 *
 */

public class EmployeeApp {

	public static void main(String[] args) {
		Employee employee1 = new Employee("Rohan","Rai",15000, 25);
		employee1.employeeInformation();
		employee1.houseRentAllowance(0.20);
		
		Employee employee2 = new Employee("Mohan","Rimal",25000, 40);
		employee2.employeeInformation();
		employee2.houseRentAllowance(0.20);
		
		//just printing out the names to check the program
		List<Employee> employee = new ArrayList<Employee>();
		employee.add(employee1);
		employee.add(employee2);
		
		for(Employee Employees: employee) {
			System.out.println(Employees.getFirstName() + " " + Employees.getLastName() );
		}
		
		//calling static method from EmployeeUtil class
		EmployeeUtil.highestSalary(employee1.getFirstName(),employee2.getFirstName(),employee1.getSalary(), employee2.getSalary());
		
		//calling static method from EmployeeUtil class
		EmployeeUtil.increaseSalary(employee1.getSalary());
		
		//calling olderEmployee method from EmployeeUtil class
		//Q.5a
		EmployeeUtil.olderEmployee(employee1,employee2);
		
		//calling updateSalary method from EmployeeUtil class
		//Q.5b
		EmployeeUtil.updateSalary(employee);
		
	    //Q.5c
	    EmployeeUtil.calculateGrossSalary(employee);
	    
	    //Q.5c
	    employee1.compareSalary(employee2);
	}
}

	

