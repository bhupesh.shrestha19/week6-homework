package com.week6.question2;

import java.util.List;

/**
 * 
 * @author Bhupesh Shrestha
 * Date: 9/25/2020
 * Q.5 Immutable Class
 *
 */

public final class ImmutableEmployee {
	
	    private final String firstName;
	    private final String lastName;
	    private final List<Employee> managedEmployees;
	    private final Employee manager;
	    private final EmploymentPeriod period;
	    private final List<PhoneNumber> phoneNumbers;
	    private final List<String> responsibilities;
	    private final int id;
	    private final double salary;
	    private final long version;
	    private final static String COMPANY_NAME;
	    private final int age;
	    
	    static {
	    	COMPANY_NAME = "XYZ Comp";
	    }
	    
		public ImmutableEmployee(String firstName, String lastName, List<Employee> managedEmployees, Employee manager,
				EmploymentPeriod period, List<PhoneNumber> phoneNumbers, List<String> responsibilities, int id,
				double salary, long version, int age) {
			super();
			this.firstName = firstName;
			this.lastName = lastName;
			this.managedEmployees = managedEmployees;
			this.manager = manager;
			this.period = period;
			this.phoneNumbers = phoneNumbers;
			this.responsibilities = responsibilities;
			this.id = id;
			this.salary = salary;
			this.version = version;
			this.age = age;
		}
		
		
		public String getFirstName() {
			return firstName;
		}
		
		public String getLastName() {
			return lastName;
		}
		
		public List<Employee> getManagedEmployees() {
			return managedEmployees;
		}
		
		public Employee getManager() {
			return manager;
		}
		
		public EmploymentPeriod getPeriod() {
			return period;
		}
		
		public List<PhoneNumber> getPhoneNumbers() {
			return phoneNumbers;
		}
		
		public List<String> getResponsibilities() {
			return responsibilities;
		}
		
		public int getId() {
			return id;
		}
		
		public double getSalary() {
			return salary;
		}
		
		public long getVersion() {
			return version;
		}
		
		public static String getCompanyName() {
			return COMPANY_NAME;
		}
		
		public int getAge() {
			return age;
		}
		

}
